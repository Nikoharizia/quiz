Petit quiz de 4 questions réalisé en html css et typescript.

J'ai d'abord commencé par créer ma page html/css avec le design que je voulais ainsi que les differents boutons 
de réponse utilisé par le quiz.

J'ai ensuite via le typescript "récupéré" tous les boutons et les zones de texte afin de mettre en relation ma page html avec 
les fonctions que j'ai créé dans typescript pour que mon quiz devienne interactif.
