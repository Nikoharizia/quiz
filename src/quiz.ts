//Tableau questions
const questionsArray: string[] = ['Combien de rayures y a-t-il sur le drapeau américain ?', 'En quelle année le métro de Londres a-t-il été ouvert ? ', 'Quel pilote a-t-il remporté le plus de championnats de Formule 1 ?', 'Quelle a été la série la plus regardée sur Netflix en 2019 ? ']

//Tableau réponses
const repArray: string[][] = [
    ['9', '11', '13', '15'], ['1861', '1863', '1865', '1867'], ['Alain Prost', 'Sebastian Vettel', 'Lewis Hamilton', 'Michael Schumacher'], [' La Casa de Papel', 'Stranger Things', 'The Umbrella cademy', 'You']
]

//Tableau avec les bonnes réponses
const goodRep: string[] = ['13', '1863', 'Lewis Hamilton', 'Stranger Things']

//Capture de tous les boutons utiles
const question = document.querySelector<HTMLElement>('#question');
const reponseBtn = document.querySelectorAll<HTMLElement>('.button');
const repN1 = document.querySelector<HTMLElement>('#n1');
const repN2 = document.querySelector<HTMLElement>('#n2');
const repN3 = document.querySelector<HTMLElement>('#n3');
const repN4 = document.querySelector<HTMLElement>('#n4');
const vraiOuFaux:any = document.querySelector<HTMLButtonElement>('#retour');
const scorefinal:any = document.querySelector<HTMLDivElement>('#score');

// Initialisation des compteurs à 0
let index = 0;
let score = 0;
let currentCorrectAnswer = '';


// Fonction qui permet de vérifier si les réponses données correpondent aux bonnes réponses
// et retourner bonne ou mauvaise réponse selon la réponse choisi (+1 pour bonne réponse)
function verifAnswer(answer: string): void {
    if (answer === currentCorrectAnswer) {
        score++;
        vraiOuFaux.style.backgroundColor = 'green';
        vraiOuFaux.innerHTML = 'Bonne réponse !';
    } else {
        vraiOuFaux.style.backgroundColor = 'red';
        vraiOuFaux.innerHTML = 'Mauvaise réponse !';
        vraiOuFaux.style.fontSize = "19px";
    }
}


// Fonction qui permet d'afficher les prochaines questions
function nextQuestion() {
    if (question) {
        question.innerHTML = questionsArray[index];
        currentCorrectAnswer = goodRep[index];
    }
}


// Fonction qui permet d'afficher prochaines réponses
function nextAnswers() {
    if (repN1 && repN2 && repN3 && repN4) {
        repN1.innerHTML = repArray[index][0];
        repN2.innerHTML = repArray[index][1];
        repN3.innerHTML = repArray[index][2];
        repN4.innerHTML = repArray[index][3];
    }
}

// Fonction pour recommencer le quiz
function recommencerQuiz() {
    index = 0;
    score = 0;
    scorefinal.style.display = 'none';
    nextQuestion();
    nextAnswers();
    vraiOuFaux.innerHTML = 'Bonne chance !';
    vraiOuFaux.style.cursor = 'pointer';
    vraiOuFaux.addEventListener('click', recommencerQuiz);
}

// Appel de fonction
nextQuestion();
nextAnswers();

// Boucle qui parcourt les éléments de reponseBtn, qui appel au click la fonction verifAnswer qui vérifie la rép donné et affiche
// un message ( bonne ou mauvaise réponse), index < questionArray.lenght -1 vérifie si il y a encore des question par rapport a l'index
// sinon recommence le quiz  

for (const rep of reponseBtn) {
    rep.addEventListener('click', (event) => {
        event.preventDefault();
        verifAnswer(rep.innerHTML);
        if (index < questionsArray.length - 1) {
            vraiOuFaux.style.display = 'block';
            index++;
            nextQuestion();
            nextAnswers();
        } else {
            vraiOuFaux.innerHTML = 'Recommencer ?';
            scorefinal.innerHTML = `Score final: ${score}/${questionsArray.length}`;
            scorefinal.style.backgroundColor ='blue'
            scorefinal.style.display = "inline";

        }
    });
}

recommencerQuiz();
























